module.exports = {
  /**
   * Windows在换行的时候，同时使用了回车符CR(carriage-return character)和换行符LF(linefeed character)
   * 而Mac和Linux系统，仅仅使用了换行符LF
   * https://blog.csdn.net/qq_27674439/article/details/111408453
   */
  endOfLine: "auto"
}