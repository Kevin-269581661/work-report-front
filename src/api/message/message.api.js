import request from "../../utils/request";
import qs from "qs";

/**
 * 消息模块 api
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "message/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "message/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 分页查询当前登录用户的消息
export const listPageSelf = (data) => {
  return request({
    url: "message/listPageSelf",
    method: "post",
    data,
  });
};

// 批量设置消息已读
export const setHasReadByIds = (data) => {
  return request({
    url: "message/setHasReadByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 获取当前登录用户的未读消息数量
export const getUnReadCount = (params) => {
  return request({
    url: "message/getUnReadCount",
    method: "get",
    params,
  });
};

// 给用户发送日报提醒
export const sendReportNoticeToUser = (data) => {
  return request({
    url: "message/sendReportNoticeToUser",
    method: "post",
    data: qs.stringify(data),
  });
};
