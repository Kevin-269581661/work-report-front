import request from "../../utils/request";
import qs from "qs";

/**
 * 版本模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "release/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "release/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 分页查询列表
export const listPage = (data) => {
  return request({
    url: "release/listPage",
    method: "post",
    data,
  });
};

// 设为标签置顶
export const toggleRoof = (data) => {
  return request({
    url: "release/toggleRoof",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询已设置为标签的版本列表
export const roofList = (params) => {
  return request({
    url: "release/roofList",
    method: "get",
    params,
  });
};
