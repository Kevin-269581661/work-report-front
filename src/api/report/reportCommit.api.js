import request from "../../utils/request";
import qs from "qs";

// 发送日报
export const sendTodayReport = (data) => {
  return request({
    url: "reportCommit/sendTodayReport",
    method: "post",
    data: qs.stringify(data),
  });
};

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "reportCommit/saveOrUpdate",
    method: "post",
    data,
  });
};

// 查询当前登录用户的今日日报提交记录数量
export const getCommitCountSelf = () => {
  return request({
    url: "reportCommit/getCommitCountSelf",
    method: "get",
  });
};

// 分页查询自己的发送日报记录
export const listPageSelf = (data) => {
  return request({
    url: "reportCommit/listPageSelf",
    method: "post",
    data,
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "reportCommit/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};
