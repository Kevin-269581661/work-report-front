import request from "../../utils/request";
import qs from "qs";

// 添加日报
export const addReport = (data) => {
  return request({
    url: "report/save",
    method: "post",
    data: qs.stringify(data),
  });
};

// 获取个人日报列表
export const getToDayReportList = (params) => {
  return request({
    url: "report/list",
    method: "get",
    params,
  });
};

// 删除日报
export const deleteReport = (data) => {
  return request({
    url: "report/delete",
    method: "post",
    data: qs.stringify(data),
  });
};

// 导出周报
export const exportWeekReport = (data) => {
  return request({
    url: "report/exportWeekReport",
    method: "post",
    data,
    responseType: "blob",
    timeout: 30000,
  });
};

// 统计一周数量和一年数量
export const getStatistics = () => {
  return request({
    url: "report/statistics",
    method: "get",
  });
};

// 分页查询列表
export const listPage = (data) => {
  return request({
    url: "report/listPage",
    method: "post",
    data,
  });
};

// 获取登录用户的上个工作日的日报列表
export const lastWorkDayReportSelf = () => {
  return request({
    url: "report/lastWorkDayReportSelf",
    method: "get",
  });
};
