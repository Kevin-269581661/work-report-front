import request from "../../utils/request";
import qs from "qs";

/**
 * 待办模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "todo/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "todo/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "todo/listPage",
    method: "post",
    data,
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "todo/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "todo/preparedData",
    method: "get",
    params,
  });
};

// 设置待办是否完成
export const toggleIsFinish = (data) => {
  return request({
    url: "todo/toggleIsFinish",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询今日待办
export const listTodayWaitDo = (params) => {
  return request({
    url: "todo/listTodayWaitDo",
    method: "get",
    params,
  });
};
