import request from "../../utils/request";
import qs from "qs";

/**
 * 任务模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "task/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "task/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "task/listPage",
    method: "post",
    data,
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "task/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "task/preparedData",
    method: "get",
    params,
  });
};
