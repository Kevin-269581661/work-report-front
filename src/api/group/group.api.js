import request from "../../utils/request";
import qs from "qs";

/**
 * 组织模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "group/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "group/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "group/listPage",
    method: "post",
    data: qs.stringify(data),
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "group/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "group/preparedData",
    method: "get",
    params,
  });
};

// 获取树形数据
export const treeList = (params) => {
  return request({
    url: "group/treeList",
    method: "get",
    params,
  });
};

// 查询自己组下的成员发送的日报记录
export const groupReportTree = (data) => {
  return request({
    url: "group/groupReportTree",
    method: "post",
    data: data,
  });
};

// 查询自己组的成员发送的日报记录
export const memberReport = (data) => {
  return request({
    url: "group/memberReport",
    method: "post",
    data: data,
  });
};

// 根据机构id查询该机构所有组织
export const listGroupByOrgId = (params) => {
  return request({
    url: "group/listGroupByOrgId",
    method: "get",
    params,
  });
};
