import request from "../../utils/request";
import qs from "qs";

/**
 * 机构模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "organization/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "organization/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 分页查询机构（需要权限）
export const listPage = (data) => {
  return request({
    url: "organization/listPage",
    method: "post",
    data,
  });
};

// 分页查询启用的机构（不需要权限）
export const listPageChoose = (data) => {
  return request({
    url: "organization/listPageChoose",
    method: "post",
    data,
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "organization/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "organization/preparedData",
    method: "get",
    params,
  });
};

// 设置机构停用/启用
export const setStatus = (data) => {
  return request({
    url: "organization/setStatus",
    method: "post",
    data: qs.stringify(data),
  });
};

// 设置机构是否可加入
export const setCanJoin = (data) => {
  return request({
    url: "organization/setCanJoin",
    method: "post",
    data: qs.stringify(data),
  });
};
