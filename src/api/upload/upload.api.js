import request from "../../utils/request";
import qs from "qs";

/**
 * 上传模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "upload/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "upload/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "upload/listPage",
    method: "post",
    data: qs.stringify(data),
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "upload/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "upload/preparedData",
    method: "get",
    params,
  });
};

// 上传单张图片
export const uploadImage = (data) => {
  return request({
    url: "upload/image",
    method: "post",
    data,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  });
};
