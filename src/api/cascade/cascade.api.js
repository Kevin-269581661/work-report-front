import request from "../../utils/request";
import qs from "qs";

// 添加级联列表数据
export const addCascade = (data) => {
  return request({
    url: "cascade/save",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询级联数据
export const getCascadeList = (params) => {
  return request({
    url: "cascade/list",
    method: "get",
    params,
  });
};

// 查询树形结构
export const listTree = (params) => {
  return request({
    url: "cascade/listTree",
    method: "get",
    params,
  });
};

// 根据机构查询所有的模块列表
export const listAll = (params) => {
  return request({
    url: "cascade/listAll",
    method: "get",
    params,
  });
};

// 删除级联列表数据
export const deleteCascade = (data) => {
  return request({
    url: "cascade/delete",
    method: "post",
    data: qs.stringify(data),
  });
};
