import request from "../../utils/request";
import qs from "qs";

// 用户注册
export const reg = (data) => {
  return request({
    url: "users/reg",
    method: "post",
    data: qs.stringify(data),
  });
};

// 退出登录
export const logout = (params) => {
  return request({
    url: "users/logout",
    method: "get",
    params,
  });
};

// 用户登录
export const login = (data) => {
  return request({
    url: "users/login",
    method: "post",
    data: qs.stringify(data),
  });
};

// 用户修改用户信息
export const usersUpdate = (data) => {
  return request({
    url: "users/update",
    method: "post",
    data: qs.stringify(data),
  });
};

// 用户修改头像
export const updateAvatar = (data) => {
  return request({
    url: "users/updateAvatar",
    method: "post",
    data: qs.stringify(data),
  });
};

// 用户修改用户密码
export const updatePassword = (data) => {
  return request({
    url: "users/updatePassword",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询所有用户列表
export const listPage = (data) => {
  return request({
    url: "users/listPage",
    method: "post",
    data: qs.stringify(data),
  });
};

// 管理员注册新用户
export const regByAdmin = (data) => {
  return request({
    url: "users/regByAdmin",
    method: "post",
    data: qs.stringify(data),
  });
};

// 管理员修改用户
export const updateByAdmin = (data) => {
  return request({
    url: "users/updateByAdmin",
    method: "post",
    data: qs.stringify(data),
  });
};

// 管理员修改用户角色
export const updateRoleByAdmin = (data) => {
  return request({
    url: "users/updateRoleByAdmin",
    method: "post",
    data: qs.stringify(data),
  });
};

// 管理员修改用户重置密码
export const updatePasswordByAdmin = (data) => {
  return request({
    url: "users/updatePasswordByAdmin",
    method: "post",
    data: qs.stringify(data),
  });
};

// 管理员修改用户停用状态
export const toggleDeletedByAdmin = (data) => {
  return request({
    url: "users/toggleDeletedByAdmin",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询所有用户列表
export const listPageByAdmin = (data) => {
  return request({
    url: "users/listPageByAdmin",
    method: "post",
    data,
  });
};

// 修改添加组织成员
export const updateUsersGroup = (data) => {
  return request({
    url: "users/updateUsersGroup",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询获取没有组织的用户列表
export const listHasNotGroupUser = (data) => {
  return request({
    url: "users/listHasNotGroupUser",
    method: "post",
    data: qs.stringify(data),
  });
};

// 当前登录用户绑定机构
export const userBindOrg = (data) => {
  return request({
    url: "users/userBindOrg",
    method: "post",
    data: qs.stringify(data),
  });
};

// 当前登录用户解除关联的机构
export const userUnboundOrg = (data) => {
  return request({
    url: "users/userUnboundOrg",
    method: "post",
    data: qs.stringify(data),
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "users/preparedData",
    method: "get",
    params,
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "users/getById",
    method: "get",
    params,
  });
};

// 添加组织成员
export const userBindGroup = (data) => {
  return request({
    url: "users/userBindGroup",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询组织成员
export const listUserByGroupId = (params) => {
  return request({
    url: "users/listUserByGroupId",
    method: "get",
    params,
  });
};

// 删除组织成员
export const userUnboundGroup = (data) => {
  return request({
    url: "users/userUnboundGroup",
    method: "post",
    data: qs.stringify(data),
  });
};

// 设置组织管理员
export const setGroupManager = (data) => {
  return request({
    url: "users/setGroupManager",
    method: "post",
    data: qs.stringify(data),
  });
};

// 获取用户配置信息
export const getUserConfig = (params) => {
  return request({
    url: "users/getUserConfig",
    method: "get",
    params,
  });
};

// 设置用户配置信息
export const setUserConfig = (data) => {
  return request({
    url: "users/setUserConfig",
    method: "post",
    data: qs.stringify(data),
  });
};

// 根据组长id，查询组员
export const getMembersByLeaderId = (params) => {
  return request({
    url: "users/getMembersByLeaderId",
    method: "get",
    params,
  });
};
