const apiObj = {};

// 懒加载
// const modulesFiles = import.meta.glob("./*/*api.js");

// for (const modulePath in modulesFiles) {
//   modulesFiles[modulePath]().then((mod) => {
//     const path = modulePath.replace(/^\.\/(.*)\.api\.\w+$/, "$1");
//     const pathArr = path.split("/");
//     const moduleName = pathArr.length ? pathArr[pathArr.length - 1] : pathArr;
//     apiObj[moduleName] = mod;
//   });
// }

const modulesFiles = import.meta.globEager("./*/*api.js");

for (const modulePath in modulesFiles) {
  const path = modulePath.replace(/^\.\/(.*)\.api\.\w+$/, "$1");
  const pathArr = path.split("/");
  const moduleName = pathArr.length ? pathArr[pathArr.length - 1] : pathArr;
  apiObj[moduleName] = modulesFiles[modulePath];
}

export default apiObj;
