import request from "../../utils/request";
import qs from "qs";

/**
 *  role 模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "role/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "role/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "role/listPage",
    method: "post",
    data,
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "role/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "role/preparedData",
    method: "get",
    params,
  });
};

// 设置启用或停用
export const toggleUse = (data) => {
  return request({
    url: "role/toggleUse",
    method: "post",
    data: qs.stringify(data),
  });
};

// 查询已启用的所有角色列表
export const getEnableList = (params) => {
  return request({
    url: "role/getEnableList",
    method: "get",
    params,
  });
};

// 获取初始化的角色名列表
export const listInitRoleName = (params) => {
  return request({
    url: "role/listInitRoleName",
    method: "get",
    params,
  });
};
