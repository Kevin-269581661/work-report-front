import request from "../../utils/request";
import qs from "qs";

/**
 *  permission 模块
 */

// 新增或修改
export const saveOrUpdate = (data) => {
  return request({
    url: "permission/saveOrUpdate",
    method: "post",
    data: qs.stringify(data),
  });
};

// 批量或单体删除
export const removeByIds = (data) => {
  return request({
    url: "permission/removeByIds",
    method: "post",
    data: qs.stringify(data),
  });
};

// 列表
export const listPage = (data) => {
  return request({
    url: "permission/listPage",
    method: "post",
    data: qs.stringify(data),
  });
};

// 详情
export const getById = (params) => {
  return request({
    url: "permission/getById",
    method: "get",
    params,
  });
};

// 预加载数据
export const preparedData = (params) => {
  return request({
    url: "permission/preparedData",
    method: "get",
    params,
  });
};

// 预加载所有的权限列表
export const getPermissionAll = (params) => {
  return request({
    url: "permission/getPermissionAll",
    method: "get",
    params,
  });
};

// 保存权限
export const saveRolePermission = (data) => {
  return request({
    url: "permission/saveRolePermission",
    method: "post",
    data: qs.stringify(data),
  });
};
