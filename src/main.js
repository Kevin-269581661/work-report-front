import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 引入Element框架
import installElementPlus from "./plugins/element";
import "./assets/css/icon.css";
// 引入表格组件
import "xe-utils";
import VXETable from "vxe-table";
import "vxe-table/lib/style.css";
// 引入鼠标右键菜单组件
import contextmenu from "vue3-contextmenu";
import "vue3-contextmenu/dist/vue3-contextmenu.css";
// 引入时间处理工具
import moment from "moment";
// 引入api模块
import api from "./api/index.js";
// 引入统一注册icon
import * as icons from "@element-plus/icons";
// 引入自定义指令
import directives from "./utils/directives";
// 引入权限编码
import permission from "./utils/permission";
// 代码高亮样式
import "highlight.js/styles/monokai-sublime.css";
import { checkAppNewVersion } from "./router";

moment.updateLocale("en", {
  week: {
    dow: 1, // 星期的第一天是星期一
    doy: 7, // 年份的第一周必须包含1月1日 (7 + 1 - 1)
  },
});

const app = createApp(App);
app.config.globalProperties.$api = api;
app.config.globalProperties.$moment = moment;
app.config.globalProperties.$permission = permission;

/**
 * 是否是超级管理员
 *
 * @returns Boolean
 */
app.config.globalProperties.$isSupperAdmin = function () {
  if (store.state.user.userInfo.isSupperAdmin) {
    return true;
  }
  return false;
};

/**
 * 是否有某个权限
 *
 * @param {*} permissionCode 权限编码
 * @returns Boolean
 */
app.config.globalProperties.$hasPermission = function (permissionCode) {
  if (store.state.user.userInfo.isSupperAdmin) {
    return true;
  }
  let has = false;
  const userPermission = store.state.user.userInfo.permissions;
  if (userPermission && userPermission.includes(permissionCode)) {
    has = true;
  }
  return has;
};

/**
 * 是否有多个权限中至少一个
 *
 * @param {*} permissionCodeList 权限编码数组
 * @returns Boolean
 */
app.config.globalProperties.$hasOneOfPermissions = function (
  permissionCodeList
) {
  if (store.state.user.userInfo.isSupperAdmin) {
    return true;
  }
  let has = false;
  const userPermission = store.state.user.userInfo.permissions;
  if (
    userPermission &&
    permissionCodeList &&
    Array.isArray(permissionCodeList)
  ) {
    has = permissionCodeList.some((item) => userPermission.includes(item));
  }
  return has;
};

/**
 * 是否有全部权限
 *
 * @param {*} permissionCodeList 权限编码数组
 * @returns Boolean
 */
app.config.globalProperties.$havePermissions = function (permissionCodeList) {
  if (store.state.user.userInfo.isSupperAdmin) {
    return true;
  }
  let has = false;
  const userPermission = store.state.user.userInfo.permissions;
  if (
    userPermission &&
    permissionCodeList &&
    Array.isArray(permissionCodeList)
  ) {
    has = permissionCodeList.every((item) => userPermission.includes(item));
  }
  return has;
};

/**
 * 全局过滤器
 */
app.config.globalProperties.$filters = {
  // 性别过滤
  sexFilter(val) {
    let name = "";
    switch (val) {
      case "1":
        name = "男";
        break;
      case "0":
        name = "女";
        break;
      default:
        name = "未填写";
    }
    return name;
  },
};

installElementPlus(app);
Object.keys(icons).forEach((key) => {
  app.component(key, icons[key]);
});

Object.keys(directives).forEach((key) => {
  app.directive(key, directives[key]);
});

app.use(store).use(router).use(VXETable).use(contextmenu).mount("#app");

// 禁止鼠标右键
window.oncontextmenu = function () {
  return false;
};
// window.onkeydown = function () {
//     if (window.event && window.event.keyCode == 123) {
//         event.keyCode = 0;
//         event.returnValue = false;
//         return false;
//     }
// };//禁止F12

/**
 * 阻止恶意调试
 */
// (() => {
//   function block() {
//     if (
//       window.outerHeight - window.innerHeight > 200 ||
//       window.outerWidth - window.innerWidth > 200
//     ) {
//       document.body.innerHTML = `
//       <h2
//         style="
//           width: 100%;
//           height: 100vh;
//           display: flex;
//           align-items: center;
//           justify-content: center;
//           color: #67C23A;
//         "
//       >
//         亲，不可以调试哦^_^，请关闭调试后刷新页面!
//       </h2>
//       `;
//     }
//     setInterval(() => {
//       (function () {
//         return false;
//       }
//         .constructor("debugger")
//         .call());
//     }, 50);
//   }
//   try {
//     process.env.NODE_ENV !== "development" && block();
//   } catch (err) {}
// })();

/*
  document.hidden
  判断页面是否隐藏的布尔值。页面隐藏包括 页面在后台标签页中 或者 浏览器最小化

  document.visibilityState
  只读属性）, 返回document的可见性，4个值：

  1.hidden：文档处于背景标签页或者窗口处于最小化状态，或者操作系统正处于 ‘锁屏状态’

  2.visible：此页面在前景标签页中，并且窗口没有最小化

  3.prerender：页面在屏幕外执行预渲染处理 document.hidden 的值为 true

  4.unloaded：页面正在从内存中卸载

  visibilitychange事件
  当标签页从可见变为不可见或者从不可见变为可见时（包括标签页切换与浏览器最小化），会触发该事件
*/
document.addEventListener("visibilitychange", function () {
  console.log("页面显示状态 ===>", document.visibilityState);
  store.commit("setPageVisible", !document.hidden);
  if (!document.hidden) {
    checkAppNewVersion();
  }
});

// 初始化时间更新器
store.commit("startUpdateDate");
