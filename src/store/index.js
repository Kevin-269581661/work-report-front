import { createStore } from "vuex";
import { isPhone } from "../utils/common";
import user from "./modules/user";
import message from "./modules/message";

import {
  SIDE_BAR_COLLAPSE,
  IS_FULL_SCREEN,
} from "../utils/keysManage/sessionKeys";
// 默认头像
import defaultAvatar from "../assets/img/default-avatar-3.png";
// import moment from "moment";

let dateUpdateTimer = null;

export default createStore({
  state: {
    tagsList: [],
    collapse: window.sessionStorage.getItem(SIDE_BAR_COLLAPSE)
      ? !!window.sessionStorage.getItem(SIDE_BAR_COLLAPSE)
      : false,
    isFullScreen: window.sessionStorage.getItem(IS_FULL_SCREEN)
      ? !!window.sessionStorage.getItem(IS_FULL_SCREEN)
      : false,
    defaultAvatar,
    // 当前时间对象
    currentDateObj: new Date(),
    // 页面显示隐藏状态
    pageVisible: !document.hidden,
    isPhone: isPhone(),
  },

  mutations: {
    setPageVisible(state, bool) {
      state.pageVisible = !!bool;
    },

    // 删除标签
    delTagsItem(state, { index }) {
      state.tagsList.splice(index, 1);
    },

    // 删除标签
    delTagsByName(state, { name }) {
      const index = state.tagsList.findIndex((item) => item.name === name);
      if (index !== -1) {
        state.tagsList.splice(index, 1);
      }
    },

    // 末尾添加标签
    setTagsItem(state, data) {
      state.tagsList.push(data);
    },

    // 清除全部标签
    clearTags(state) {
      state.tagsList = [];
    },

    // 设置特定标签，关闭其他标签
    closeTagsOther(state, data) {
      state.tagsList = data;
    },

    // 设置标签列表
    setTagsList(state, tagsList) {
      state.tagsList = tagsList;
    },

    // 关闭当前标签
    closeCurrentTag(state, { route, router }) {
      for (let i = 0, len = state.tagsList.length; i < len; i++) {
        const item = state.tagsList[i];
        if (item.fullPath === route.fullPath) {
          if (i < len - 1) {
            router.push(state.tagsList[i + 1].path);
          } else if (i > 0) {
            router.push(state.tagsList[i - 1].path);
          } else {
            router.push("/");
          }
          state.tagsList.splice(i, 1);
          break;
        }
      }
    },

    // 侧边栏折叠
    handleCollapse(state, data) {
      const bool = !!data;
      state.collapse = bool;
      const val = bool ? "1" : "";
      window.sessionStorage.setItem(SIDE_BAR_COLLAPSE, val);
    },

    // 切换全屏
    toggleFullScreen(state, isFull) {
      const bool = !!isFull;
      state.isFullScreen = bool;
      const val = bool ? "1" : "";
      window.sessionStorage.setItem(IS_FULL_SCREEN, val);
    },

    /**
     * 设置/更新当前时间对象
     */
    startUpdateDate(state) {
      dateUpdateTimer = setInterval(() => {
        state.currentDateObj = new Date();
        // console.log(state.currentDateObj);
      }, 1000);
    },
  },
  actions: {},
  getters: {
    // currentDate: (state) => {
    //   return moment(state.currentDateObj).format("YYYY-MM-DD");
    // },
    // currentDateTime: (state) => {
    //   return moment(state.currentDateObj).format("YYYY-MM-DD HH:mm:ss");
    // },
  },
  modules: {
    user,
    message,
  },
});
