import { createRouter, createWebHashHistory } from "vue-router";
import { USER_INFO } from "../utils/keysManage/localKeys";
import { Session } from "../utils/storage";

import * as sessionKeys from "../utils/keysManage/sessionKeys";

import axios from "axios";

import Main from "../views/common/Main.vue";
import Login from "../views/common/Login.vue";
import NotFind from "../views/common/notFind.vue";
import ChooseOrg from "../views/common/ChooseOrg.vue";

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
  },
  {
    path: "/login",
    name: "login",
    meta: {
      title: "登录",
    },
    component: Login,
  },
  {
    path: "/ChooseOrg",
    name: "ChooseOrg",
    meta: {
      title: "选择机构",
    },
    component: ChooseOrg,
  },
  {
    path: "/notFind",
    name: "notFind",
    meta: {
      title: "404",
    },
    component: NotFind,
  },
  {
    path: "/",
    name: "Main",
    component: Main,
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        meta: {
          title: "系统首页",
        },
        component: () =>
          import(
            /* webpackChunkName: "dashboard" */ "../views/home/Dashboard.vue"
          ),
      },
      /**
       * 日报模块 report
       */
      {
        path: "/reportSave",
        name: "reportSave",
        meta: {
          title: "发布日报",
          // permission: "report:saveOrUpdate",
        },
        component: () =>
          import(
            /* webpackChunkName: "reportSave" */ "../views/report/reportSave.vue"
          ),
      },
      {
        path: "/reportTable",
        name: "reportTable",
        meta: {
          title: "我的日报",
          // permission: "report:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "reportTable" */ "../views/report/reportTable.vue"
          ),
      },
      {
        path: "/ReportManage",
        name: "ReportManage",
        meta: {
          title: "日报管理",
          // permission: "report:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "ReportManage" */ "../views/report/ReportManage.vue"
          ),
      },
      {
        path: "/SendedReportManage",
        name: "SendedReportManage",
        meta: {
          title: "已发送日报管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "SendedReportManage" */ "../views/report/SendedReportManage.vue"
          ),
      },
      {
        path: "/TodayReportManage",
        name: "TodayReportManage",
        meta: {
          title: "日报汇总",
        },
        component: () =>
          import(
            /* webpackChunkName: "TodayReportManage" */ "../views/report/TodayReportManage.vue"
          ),
      },
      {
        path: "/reportCommitForm",
        name: "reportCommitForm",
        meta: {
          title: "发送日报",
        },
        component: () =>
          import(
            /* webpackChunkName: "reportCommitForm" */ "../views/report/reportCommitForm.vue"
          ),
      },
      /**
       * 任务模块 task
       */
      {
        path: "/TaskTable",
        name: "TaskTable",
        meta: {
          title: "我的任务",
          // permission: "task:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "TaskTable" */ "../views/task/TaskTable.vue"
          ),
      },
      {
        path: "/TaskManage",
        name: "TaskManage",
        meta: {
          title: "任务管理",
          // permission: "task:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "TaskManage" */ "../views/task/TaskManage.vue"
          ),
      },
      {
        path: "/ReleaseManage",
        name: "ReleaseManage",
        meta: {
          title: "版本管理",
          // permission: "release:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "ReleaseManage" */ "../views/task/ReleaseManage.vue"
          ),
      },
      {
        path: "/TaskView",
        name: "TaskView",
        meta: {
          title: "任务视图",
          // permission: "task:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "TaskView" */ "../views/task/TaskView.vue"
          ),
      },
      /**
       * 系统模块 sys
       */
      {
        path: "/ProjectModuleManage",
        name: "ProjectModuleManage",
        meta: {
          title: "模块管理",
          // permission: "cascade:saveOrUpdate",
        },
        component: () =>
          import(
            /* webpackChunkName: "ProjectModuleManage" */ "../views/sys/ProjectModuleManage.vue"
          ),
      },
      {
        path: "/GroupManage",
        name: "GroupManage",
        meta: {
          title: "组织架构",
        },
        component: () =>
          import(
            /* webpackChunkName: "GroupManage" */ "../views/sys/GroupManage.vue"
          ),
      },
      {
        path: "/OrgManage",
        name: "OrgManage",
        meta: {
          title: "机构管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "OrgManage" */ "../views/organization/OrgManage.vue"
          ),
      },
      /**
       * 用户模块 user
       */
      {
        path: "/UserManage",
        name: "UserManage",
        meta: {
          title: "用户管理",
          // permission: "users:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "UserManage" */ "../views/user/UserManage.vue"
          ),
      },
      /**
       * 角色模块 role
       */
      {
        path: "/RoleManage",
        name: "RoleManage",
        meta: {
          title: "角色管理",
          // permission: "role:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "RoleManage" */ "../views/role/RoleManage.vue"
          ),
      },
      {
        path: "/PermissionManage",
        name: "PermissionManage",
        meta: {
          title: "权限管理",
          // permission: "permission:listPage",
        },
        component: () =>
          import(
            /* webpackChunkName: "PermissionManage" */ "../views/role/PermissionManage.vue"
          ),
      },
      /**
       * 消息模块
       */
      {
        path: "/MessageTable",
        name: "MessageTable",
        meta: {
          title: "消息列表",
        },
        component: () =>
          import(
            /* webpackChunkName: "MessageTable" */ "../views/message/MessageTable.vue"
          ),
      },
      /**
       * 其他模块
       */
      {
        path: "/TodoManage",
        name: "TodoManage",
        meta: {
          title: "待办管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "TodoManage" */ "../views/todo/TodoManage.vue"
          ),
      },
      {
        path: "/TodoDetail",
        name: "TodoDetail",
        meta: {
          title: "待办详情",
        },
        component: () =>
          import(
            /* webpackChunkName: "TodoDetail" */ "../views/todo/TodoDetail.vue"
          ),
      },
      {
        path: "/addProject",
        name: "addProject",
        meta: {
          title: "添加项目",
          // permission: "cascade:saveOrUpdate",
        },
        component: () =>
          import(
            /* webpackChunkName: "addProject" */ "../views/AddProject.vue"
          ),
      },
      {
        path: "/table",
        name: "basetable",
        meta: {
          title: "表格",
        },
        component: () =>
          import(/* webpackChunkName: "table" */ "../views/BaseTable.vue"),
      },
      {
        path: "/charts",
        name: "basecharts",
        meta: {
          title: "图表",
        },
        component: () =>
          import(/* webpackChunkName: "charts" */ "../views/BaseCharts.vue"),
      },
      {
        path: "/form",
        name: "baseform",
        meta: {
          title: "表单",
        },
        component: () =>
          import(/* webpackChunkName: "form" */ "../views/BaseForm.vue"),
      },
      {
        path: "/tabs",
        name: "tabs",
        meta: {
          title: "tab标签",
        },
        component: () =>
          import(/* webpackChunkName: "tabs" */ "../views/Tabs.vue"),
      },
      {
        path: "/donate",
        name: "donate",
        meta: {
          title: "2022加油",
        },
        component: () =>
          import(/* webpackChunkName: "donate" */ "../views/Donate.vue"),
      },
      {
        path: "/permission",
        name: "permission",
        meta: {
          title: "权限管理",
        },
        component: () =>
          import(
            /* webpackChunkName: "permission" */ "../views/Permission.vue"
          ),
      },
      {
        path: "/i18n",
        name: "i18n",
        meta: {
          title: "国际化语言",
        },
        component: () =>
          import(/* webpackChunkName: "i18n" */ "../views/I18n.vue"),
      },
      {
        path: "/upload",
        name: "upload",
        meta: {
          title: "上传插件",
        },
        component: () =>
          import(/* webpackChunkName: "upload" */ "../views/Upload.vue"),
      },
      {
        path: "/icon",
        name: "icon",
        meta: {
          title: "自定义图标",
        },
        component: () =>
          import(/* webpackChunkName: "icon" */ "../views/Icon.vue"),
      },
      {
        path: "/user",
        name: "user",
        meta: {
          title: "个人中心",
        },
        component: () =>
          import(/* webpackChunkName: "user" */ "../views/user/User.vue"),
      },
      {
        path: "/editor",
        name: "editor",
        meta: {
          title: "富文本编辑器",
        },
        component: () =>
          import(/* webpackChunkName: "editor" */ "../views/Editor.vue"),
      },
      {
        path: "/403",
        name: "403",
        meta: {
          title: "没有权限",
        },
        component: () =>
          import(/* webpackChunkName: "403" */ "../views/403.vue"),
      },
      {
        path: "/404",
        name: "404",
        meta: {
          title: "找不到页面",
        },
        component: () =>
          import(/* webpackChunkName: "404" */ "../views/404.vue"),
      },
    ],
  },
  {
    // 没有权限和找不到对应菜单页面
    path: "/:pathMatch(.*)",
    redirect: "/notFind",
    component: NotFind,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = `日报系统 | ${to.meta.title}`;
  const userInfoStr = localStorage.getItem(USER_INFO);
  const userInfo = userInfoStr ? JSON.parse(userInfoStr) : null;
  if (!userInfo && to.path !== "/login") {
    next("/login");
  } else if (userInfo && !userInfo.orgId && !userInfo.isSupperAdmin) {
    if (to.path === "/login" || to.path === "/ChooseOrg") {
      next();
    } else {
      next("/ChooseOrg");
    }
  } else if (to.meta.permission) {
    if (userInfo.isSupperAdmin) {
      next();
    } else if (
      userInfo.permissions &&
      userInfo.permissions.includes(to.meta.permission)
    ) {
      next();
    } else {
      next("/403");
    }
  } else {
    next();
  }
  // 检查版本更新
  if (from.path !== "/") {
    checkAppNewVersion();
  }
});

// 检查服务端是否已经更新，如果更新刷新页面
export async function checkAppNewVersion() {
  const url = `/version.json?t=${Date.now()}`;
  let res = null;
  try {
    res = await axios.get(url);
  } catch (err) {
    console.error("checkAppNewVersion error: ", err);
  }
  if (!res) return;
  const version = res.data.version;
  const localVersion = Session.get(sessionKeys.APP_VERSION);
  if (localVersion && localVersion !== version) {
    Session.set(sessionKeys.APP_VERSION, version);
    window.location.reload();
  }
  Session.set(sessionKeys.APP_VERSION, version);
}

checkAppNewVersion()

export default router;
