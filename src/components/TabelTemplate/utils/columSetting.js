/** 表格参数设置 */
const tableSetting = {
  fontWidth: 15, // 表格单字宽度
  thInputWidth: 40, // 预留切换为表单控件时的拓展宽度
  otherWidth: 60, // 左右预留边距如：排序
  thPading: 10, // 单元格的内边距（暂未全局控制，不可更改！！！）
};
export const getColumMinWidth = (title, type = "default", boolean = false) => {
  const typeMap = {
    input: inputWidth,
    select: inputWidth,
    "date-picker": datePickerWidth,
    "date-picker-time": datePickerTimeWidth,
    "date-picker-two": datePickerTwoWidth,
    "date-picker-two-time": datePickerTwoTimeWidth,
    "input-two": inputTwoWidth,
    default: defaultWidth,
  };
  return typeMap[type](title, boolean);
};
/**
 * 普通文字表头
 * @title 必传 - 表头标题
 */
function defaultWidth(title) {
  const titleNum = getStrLength(title);
  return getDefaultWidth(titleNum);
}
/**
 * 输入型、选项型
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function inputWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  const width = getDefaultWidth(titleNum);
  return boolean ? getShowInputWidth(width) : width;
}
/**
 * 输入型区间筛选
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function inputTwoWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  const width = getDefaultWidth(titleNum);
  return boolean ? getShowInputTwoWidth(width) : width;
}
/**
 * 日期 yyyy-MM-dd
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function datePickerWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  const width = getDefaultWidth(titleNum);
  return boolean ? formDateWidth(width) : width;
}
/**
 * 日期 yyyy-MM-dd HH:mm:ss
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function datePickerTimeWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  let width = getDefaultWidth(titleNum);
  const timeWidth = 186; // 最小宽度
  if (width < timeWidth) width = timeWidth;
  return boolean ? getShowInputWidth(width) : width;
}
/**
 * 日期区间 yyyy-MM-dd
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function datePickerTwoWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  const width = getDefaultWidth(titleNum);
  return boolean ? formDateWidth(width) : width;
}
/**
 * 日期区间 yyyy-MM-dd HH:mm:ss
 * @boolean 必传 - true时为显示表单宽度 false为普通显示
 * @title 必传 - 表头标题
 */
function datePickerTwoTimeWidth(title, boolean = false) {
  const titleNum = getStrLength(title);
  let width = getDefaultWidth(titleNum);
  const timeWidth = 186; // 最小宽度
  if (width < timeWidth) width = timeWidth;
  return boolean ? formDateWidth(width) : width;
}

/** 宽度计算方法 */
const getStrLength = (str) => {
  // 计算字符长度 - 两个英文字符 = 1
  if (typeof str !== "string") return 0;
  const arr = str.match(/[^\x00-\x80]/g) || [];
  let codeNum = (str.length - arr.length) / 2;
  codeNum = Math.ceil(codeNum);
  return arr.length + codeNum;
};
const getDefaultWidth = (num) => {
  // 换算宽度
  return (
    tableSetting.fontWidth * Number(num) +
    tableSetting.thPading * 2 +
    tableSetting.otherWidth
  );
};
const getShowInputWidth = (width) => {
  // 显示输入框、下拉选项时的宽度拓展
  return width + tableSetting.thInputWidth;
};
const getShowInputTwoWidth = (width) => {
  // 显示两个输入框时的宽度拓展
  return width * 2 + tableSetting.thInputWidth;
};
const formDateWidth = (width) => {
  // 显示日期区间选择时的宽度拓展
  return width * 2 + tableSetting.thInputWidth;
};
