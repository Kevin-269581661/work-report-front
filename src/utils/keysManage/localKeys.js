/**
 * localStorage 的 key
 */

export const USER_INFO = "userInfo";

export const REMEMBER_PASSWORD = "rememberPassword";

export const TOKEN = "token";

// 今天的日期
export const TODAY_DATE = "todayDate";
