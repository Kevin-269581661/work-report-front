/**
 * session 的 key
 */

// 是否折叠菜单
export const SIDE_BAR_COLLAPSE = "sideBarCollapse";

// 是否全屏
export const IS_FULL_SCREEN = "isFullScreen";

// 消息未读数量
export const MSG_UNREAD_COUNT = "messageUnreadCount";

export const APP_VERSION = "appVersion";
