import CryptoJS from "crypto-js";

const aseKey = "nizuiniu"; // 秘钥必须为：8/16/32位

// 加密
export function encryptData(message) {
  if (typeof message !== "string") {
    console.error("加密参数必须为 string 类型");
    return "";
  }
  if (!message) {
    return "";
  }
  const encrypt = CryptoJS.AES.encrypt(
    message,
    CryptoJS.enc.Utf8.parse(aseKey),
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    }
  ).toString();
  // console.log("加密后数据-", encrypt);
  return encrypt;
}

// 解密
export function decryptData(message) {
  if (typeof message !== "string") {
    console.error("解密参数必须为 string 类型");
    return "";
  }
  if (!message) {
    return "";
  }
  const decrypt = CryptoJS.AES.decrypt(
    message,
    CryptoJS.enc.Utf8.parse(aseKey),
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    }
  ).toString(CryptoJS.enc.Utf8);
  // console.log("解密后数据-", decrypt);
  return decrypt;
}

// MD5摘要加密
export function MD5decryptData(message) {
  const decrypt = CryptoJS.MD5(message).toString(CryptoJS.enc.Utf8);
  // console.log("MD5摘要加密-", decrypt);
  return decrypt;
}
