export default {
  /**
   *  用户模块 users
   */
  USERS_PAGE: "users:listPage", // "查看列表"
  USERS_UPDATE_GROUP: "users:updateUsersGroup", // "修改组织成员"
  USERS_REG_BY_ADMIN: "users:regByAdmin", // "管理员注册用户"
  USERS_UPDATE_BY_ADMIN: "users:updateByAdmin", // "管理员修改用户信息"
  USERS_UPDATE_ROLE_BY_ADMIN: "users:updateRoleByAdmin", // "管理员修改用户角色"
  USERS_UPDATE_PW_BY_ADMIN: "users:updatePasswordByAdmin", // "管理员修改用户密码"
  USERS_UPDATE_STATUS_BY_ADMIN: "users:toggleDeletedByAdmin", // "管理员修改用户停用状态"

  /**
   * 任务模块 task
   */
  TASK_PAGE: "task:listPage", // "查看列表"
  TASK_SAVE_OR_UPDATE: "task:saveOrUpdate", // "新增或者修改"
  TASK_DELETE: "task:removeByIds", // "删除"

  /**
   * 版本模块 release
   */
  RELEASE_PAGE: "release:listPage", // "查看列表"
  RELEASE_SAVE_OR_UPDATE: "release:saveOrUpdate", // "新增或者修改"
  RELEASE_DELETE: "release:removeByIds", // "删除"
  RELEASE_TOGGLE_ROOF: "release:toggleRoof", // "设为标签置顶"

  /**
   * 角色模块 role
   */
  ROLE_PAGE: "role:listPage", // "查看列表"
  ROLE_SAVE_OR_UPDATE: "role:saveOrUpdate", // "新增或者修改"
  ROLE_GET_BY_ID: "role:getById", // "获取详情"
  ROLE_DELETE: "role:removeByIds", // "删除"
  ROLE_TOGGLE_USE: "role:toggleUse", // "设置是否启用"

  /**
   * 日报模块 report
   */
  REPORT_PAGE: "report:listPage", // "查看列表"
  REPORT_SAVE_OR_UPDATE: "report:saveOrUpdate", // "新增或者修改"
  REPORT_DELETE: "report:removeByIds", // "删除"
  REPORT_EXPORT: "report:exportWeekReport", // "导出周报"
  REPORT_SEND: "report:sendTodayReport", // "发送日报"
  REPORT_COMMIT_PAGE: "report:reportCommitPage", // "查看发送日报列表"
  REPORT_COMMIT_DELETE: "report:reportCommitDelete", // "删除发送日报"

  /**
   * 权限模块 permission
   */
  PERMISSION_PAGE: "permission:listPage", // "查看列表"
  PERMISSION_SAVE_OR_UPDATE: "permission:saveOrUpdate", // "新增或者修改"
  PERMISSION_DELETE: "permission:removeByIds", // "删除"

  /**
   * 组织架构模块 group
   */
  GROUP_PAGE: "group:listPage", // "查看列表"
  GROUP_SAVE_OR_UPDATE: "group:saveOrUpdate", // "新增或者修改"
  GROUP_DELETE: "group:removeByIds", // "删除"
  GROUP_REPORT_TREE: "group:groupReportTree", // "查看日报汇总"

  /**
   * 项目级联模块 cascade
   */
  CASCADE_PAGE: "cascade:listPage", // "查看列表"
  CASCADE_SAVE_OR_UPDATE: "cascade:saveOrUpdate", // "新增或者修改"
  CASCADE_DELETE: "cascade:removeByIds", // "删除"

  /**
   * 机构模块 organization
   */
  ORG_PAGE: "organization:listPage", // "查看列表"
  ORG_SAVE_OR_UPDATE: "organization:saveOrUpdate", // "新增或者修改"
  ORG_DELETE: "organization:removeByIds", // "删除"
  ORG_SET_STATUS: "organization:setStatus", // "设置机构停用/启用"
  ORG_SET_CAN_JOIN: "organization:setCanJoin", // "设置机构是否可加入"
};
