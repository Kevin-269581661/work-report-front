import config from "../config/index";

export default {
  "dialog-hover": {
    mounted(el, binding) {
      const dialog = el.querySelector(".el-dialog");
      if (dialog) {
        dialog.style.transform = `perspective(500px) scale(1)`;
        dialog.style.transition = "transform 0.1s linear";
        dialog.addEventListener("mousemove", function (event) {
          const relX = (event.offsetX + 1) / dialog.offsetWidth;
          const relY = (event.offsetY + 1) / dialog.offsetHeight;
          const degX = (relX - 0.5) * 1.5;
          const degY = (relY - 0.5) * 1.5;
          const rotY = `rotateY(${degX}deg)`;
          const rotX = `rotateX(${degY}deg)`;
          dialog.style.transform = `perspective(500px) scale(1) ${rotY} ${rotX}`;
        });
        dialog.addEventListener("mouseleave", function (event) {
          dialog.style.transform = `perspective(500px) scale(1)`;
        });
      }
    },
  },
};
