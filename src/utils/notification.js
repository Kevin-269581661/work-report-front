/* -------------------- 浏览器通知 --------------------- */

import { ElMessage, ElMessageBox } from "element-plus";

/**
 * 检查浏览器通知是否开启
 */
export function checkBrowserNotification() {
  // 检查浏览器是否支持浏览器通知
  if (!window.Notification) {
    return null;
  }
  // 检查浏览器通知权限是否开启
  // default：用户还没有做出任何许可，因此不会弹出通知。
  // granted：用户明确同意接收通知。
  // denied：用户明确拒绝接收通知。
  console.log("Notification.permission", Notification.permission);
  if (Notification.permission === "denied") {
    ElMessageBox.alert(
      "检测到您的浏览器通知未打开，为了本系统给您更好的体验，建议在浏览器设置中允许通知",
      "温馨提示",
      {
        confirmButtonText: "好的",
      }
    );
  } else if (Notification.permission === "default") {
    ElMessageBox.confirm(
      "为了更好的用户体验，请允许我们发送一些必要的通知",
      "温馨提示",
      {
        confirmButtonText: "允许",
        cancelButtonText: "不了，下次吧",
        type: "warning",
      }
    )
      .then(() => {
        // 尝试请求用户授权
        Notification.requestPermission(function (status) {
          if (status === "granted") {
            ElMessage.success("设置成功，感谢您的使用");
          } else {
            // 用户拒绝接收通知
            console.error("用户拒绝接收通知");
          }
        });
      })
      .catch(() => {});
  }
}

/**
 * 检查是否可以使用浏览器通知
 */
export function canUseBrowserNotification() {
  if (window.Notification && Notification.permission === "granted") {
    return true;
  } else {
    return false;
  }
}

/**
 * 显示浏览器通知
 */
export function showBrowserNotification({ title, content }) {
  if (window.Notification && Notification.permission !== "denied") {
    Notification.requestPermission(function (status) {
      if (status === "granted") {
        const n = new Notification(title, {
          body: content,
        });
        // 实例对象的事件
        /*
              show：通知显示给用户时触发。
              click：用户点击通知时触发。
              close：用户关闭通知时触发。
              error：通知出错时触发（大多数发生在通知无法正确显示时）。
            
            */
        // n.onshow = function() {
        //     console.log('Notification shown');
        // };
        n.click = function () {
          console.log("Notification click");
          n.close.bind(n);
        };

        // close方法
        // Notification实例的close方法用于关闭通知。

        // 手动关闭
        // n.close();
        // 自动关闭
        // n.onshow = function () {
        //   setTimeout(n.close.bind(n), 10000);
        // };
      } else {
        // 如果用户拒绝接收通知，可以用alert方法代替
        console.log("用户拒绝接收通知");
      }
    });
  } else {
    console.log("浏览器不支持或用户拒绝接收通知");
  }
}
