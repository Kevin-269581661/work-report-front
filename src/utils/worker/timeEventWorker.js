/**
 * timeEventWorker 子线程
 */
let dateUpdateTimer = null;
let eventTimeList = [];

/**
 * 事件监听
 */
onmessage = function (e) {
  eventTimeList = e.data;
  console.log("timeEventWorker 子线程 ==> eventTimeList", eventTimeList);
  if (eventTimeList.length) {
    // 开启扫描
    if (dateUpdateTimer) {
      clearInterval(dateUpdateTimer);
    }
    dateUpdateTimer = setInterval(() => {
      console.log("timeEventWorker 子线程 ==>");
      eventTimeList.forEach((item) => {
        if (new Date().getTime() >= new Date(item).getTime()) {
          postMessage(item);
        }
      });
    }, 1000);
  } else {
    clearInterval(dateUpdateTimer);
    dateUpdateTimer = null;
  }
};
