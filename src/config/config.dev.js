/* ----------------------  开发环境-配置文件 ----------------------- */

export default {
  /**
   * 请求路径
   */
  baseURL: "/api",

  /**
   * 文件访问路径（文件被指定保存至本地文件夹，开发环境需要配置静态资源访问路径才可访问到）
   */
  fileDownloadUrl: "http://localhost:8999",

  /**
   * webSocket 请求路径
   */
  webSocketUrl: "http://localhost:8999",
};
