/* --------------------- 生产环境-配置文件 ---------------------- */

export default {
  /**
   * 请求路径
   */
  baseURL: "https://workreport.yunfengzhijia.cn/api",

  /**
   * 文件访问路径（通过nginx配置图片资源访问路径）
   */
  fileDownloadUrl: "https://workreport.yunfengzhijia.cn/upload",

  /**
   * webSocket 请求路径
   */
  webSocketUrl: "https://workreport.yunfengzhijia.cn/api",
};
