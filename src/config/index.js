import devConfig from "./config.dev";
import proConfig from "./config.pro";

const config = proConfig;

export default {
  ...config,

  /**
   * 上传文件业务分类
   */
  businessKey: {
    AVATAR: { val: "1", label: "头像" },
    TODO: { val: "2", label: "待办" },
    TASK: { val: "3", label: "任务" },
  },
};
