import vue from "@vitejs/plugin-vue";

const path = require("path");

// console.log(path.resolve(__dirname, './src'))

export default {
  base: "/",
  // build: {
  //   rollupOptions: {
  //     external: ["vue", "vuetify", "wangEditor"],
  //   },
  // },
  plugins: [vue()],
  "resolve.alias": {
    // 键必须以斜线开始和结束
    "/@/": path.resolve(__dirname, "./src"),
  },
  optimizeDeps: {
    include: ["schart.js"],
  },
  // 反向代理，此处应该特别注意，网上很多教程是直接设置proxy，并没有向官网那样添加 server，可能会导致失败，
  // vite官网：https://vitejs.dev/guide/features.html#async-chunk-loading-optimization
  server: {
    proxy: {
      "/api": {
        target: "http://localhost:8999",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
};
